import "babel-polyfill";
import * as PIXI from "pixi.js";

PIXI.utils.skipHello();

/*
 * constants
 * 	gameBgColor: game background color
 *	circleColor: circle figure color
 *	squareColor: square figure color
 *	triangleColor: triangle figure color
 *	shapeTypes: types of shapes enum
 *
 * 	gs:  game size in pixels
 * 	ms:  movement speed of a figure
 * 	fs:  figure size in pixels
 *	fc:  figure center in pixels
 * 	d:   matrix dimensions
 */
const gameBgColor = 0xf8bbd0;
const circleColor = 0xab47bc;
const squareColor = 0xec407a;
const triangleColor = 0x3f51b5;
const borderColor = 0xfafafa;

const shapeTypes = ["square", "circle", "triangle"];

const gs = 500;
const ms = 15;
const fs = 50;
const fc = fs / 2;
const d = 10;

/*
 * globals
 *	lastRandomSequence: last random generated sequence, helps shuffle figures a bit.
 *	pickedup:	    element in move
 *	scoreEl:	    score html element
 * 	matrix:		    d*d row-wise matrix world
 *	score:		    game score
 * 	game:   	    pixi application
 *	done:   	    are we done playing?
 */
let lastRandomSequence;
let pickedup;
let scoreEl;
let matrix;
let score;
let game;
let done;

/*
 * range - creates array ranging from [start to stop]
 * @start: first digit in the sequence inclusive
 * @stop: last digit in the sequence inclusive
 * @step: moving step in the sequence creation
 *
 * returns sequence from start inclusive to finish inclusive
 * with step distance between them in the array
 */
const range = (start, stop, step) =>
	Array.from(
		{ length: (stop - start) / step + 1 },
		(_, i) => start + i * step
	);

/*
 * figureCreate - instantiating figure, places it in matrix & draws it
 * @type: figure type ["circle" | "square" | "triangle"]
 * @x: x coordinate in 2d matrix
 * @y: y coordinate in 2d matrix
 *
 * Instantiate figure object of the given type
 * place it in the matrix @(x,y).
 * Draw object, frame, and give it hitbox
 * make it interactive
 */
const figureCreate = (type, x, y) => {
	const self = {
		type,
		x,
		y,
		sinking: false
	};

	self.Graphics = new PIXI.Graphics();
	self.Graphics.position.set(x * fs, y * fs);
	self.Graphics.hitArea = new PIXI.Rectangle(0, 0, fs, fs);

	self.Graphics.interactive = true;
	self.Graphics.on("click", e => {
		if (pickedup) {
			let isMoveLegal = false;
			/* [right, left, top, bottom] */
			let legalMoves = [
				pickedup.x + pickedup.y * d + 1,
				pickedup.x + pickedup.y * d - 1,
				pickedup.x + pickedup.y * d - d,
				pickedup.x + pickedup.y * d + d
			];
			let move = legalMoves.indexOf(self.x + self.y * d);
			isMoveLegal = move !== -1;
			if (move === 0) isMoveLegal = pickedup.x < 9;
			if (move === 1) isMoveLegal = pickedup.x > 0;
			if (move === 2) isMoveLegal = pickedup.y > 0;
			if (move === 3) isMoveLegal = pickedup.y < 9;

			if (!isMoveLegal) {
				pickedup.Graphics.alpha = 1;
				pickedup = false;
				return;
			}
			/*
			 * find amount of chains at the time
			 * try the move out, allow it if it leads to more chains
			 */
			let chlen = findChains().length;
			let _t = matrix[pickedup.x + pickedup.y * d];
			matrix[pickedup.x + pickedup.y * d] =
				matrix[self.x + self.y * d];
			matrix[self.x + self.y * d] = _t;

			let _chlen = findChains().length;
			_t = matrix[pickedup.x + pickedup.y * d];
			matrix[pickedup.x + pickedup.y * d] =
				matrix[self.x + self.y * d];
			matrix[self.x + self.y * d] = _t;

			if (_chlen <= chlen) {
				pickedup.Graphics.alpha = 1;
				pickedup = false;
				return;
			}

			/* actual swap */
			matrix[pickedup.x + pickedup.y * d] = self;
			let tx = self.x;
			let ty = self.y;
			let _tx = self.Graphics.x;
			let _ty = self.Graphics.y;
			matrix[pickedup.x + pickedup.y * d].x = pickedup.x;
			matrix[pickedup.x + pickedup.y * d].y = pickedup.y;
			matrix[pickedup.x + pickedup.y * d].Graphics.x =
				pickedup.Graphics.x;
			matrix[pickedup.x + pickedup.y * d].Graphics.y =
				pickedup.Graphics.y;

			matrix[tx + ty * d] = pickedup;
			matrix[tx + ty * d].x = tx;
			matrix[tx + ty * d].y = ty;
			matrix[tx + ty * d].Graphics.x = _tx;
			matrix[tx + ty * d].Graphics.y = _ty;
			matrix[tx + ty * d].Graphics.alpha = 1;

			pickedup = false;
			return;
		}

		pickedup = matrix[self.x + self.y * d];
		pickedup.Graphics.alpha = 0.7;
	});

	({
		circle: () => {
			self.Graphics.beginFill(circleColor);
			self.Graphics.drawCircle(fc, fc, fc);
			self.points = 3;
		},
		square: () => {
			self.Graphics.beginFill(squareColor);
			self.Graphics.drawRect(0, 0, fs, fs);
			self.points = 2;
		},
		triangle: () => {
			self.Graphics.beginFill(triangleColor);
			self.Graphics.moveTo(0, fs);
			self.Graphics.lineTo(fs / 2, 0);
			self.Graphics.lineTo(fs, fs);
			self.Graphics.lineTo(0, fs);
			self.points = 1;
		}
	}[type]());
	self.Graphics.endFill();
	self.Graphics.lineStyle(1, borderColor, 0.18, 0);
	self.Graphics.moveTo(0, 0);
	self.Graphics.lineTo(fs, 0);
	self.Graphics.lineTo(fs, fs);
	self.Graphics.lineTo(0, fs);
	self.Graphics.lineTo(0, 0);

	game.stage.addChild(self.Graphics);

	matrix[x + y * d] = self;

	return self;
};

/*
 * figureSink - sinks figure one level unless there is collision
 * @index: index of the figure in the world
 *
 * Sinks figure one level down unless there is collision
 * with either another object or worlds bottom
 */
const figureSink = index => {
	if (!matrix[index]) return;
	let nextIndex;

	matrix[index].sinking =
		matrix[index].y < d - 1 &&
		!matrix[matrix[index].x + (matrix[index].y + 1) * d];

	if (matrix[index].sinking) {
		nextIndex = matrix[index].x + (matrix[index].y + 1) * d;
		matrix[nextIndex] = matrix[index];
		matrix[nextIndex].y = matrix[index].y + 1;
		matrix[index] = 0;
	}
};

/* findChains - finds chains in the matrix
 *
 * traverses matrix row-wise & column-wise
 * finds & returns chains of matches
 */
const findChains = () => {
	let staged = [];

	/* horizontal */
	for (let i = 0; i < d; i++) {
		for (let j = 0; j < d - 2; j++) {
			if (
				!matrix[j + i * d] ||
				!matrix[j + 1 + i * d] ||
				!matrix[j + 2 + i * d]
			)
				continue;
			if (
				matrix[j + i * d].type ===
					matrix[j + 1 + i * d].type &&
				matrix[j + i * d].type ===
					matrix[j + 2 + i * d].type
			) {
				staged.push(j + i * d);
				staged.push(j + 1 + i * d);
				staged.push(j + 2 + i * d);
			}
		}
	}

	/* vertical */
	for (let i = 0; i < d; i++) {
		for (let j = 0; j < d - 2; j++) {
			if (
				!matrix[j * d + i] ||
				!matrix[(j + 1) * d + i] ||
				!matrix[(j + 2) * d + i]
			)
				continue;

			if (
				matrix[j * d + i].type ===
					matrix[(j + 1) * d + i].type &&
				matrix[j * d + i].type ===
					matrix[(j + 2) * d + i].type
			) {
				staged.push(j * d + i);
				staged.push((j + 1) * d + i);
				staged.push((j + 2) * d + i);
			}
		}
	}

	staged = staged.filter((e, i, a) => a.indexOf(e) == i);
	return staged;
};

/*
 * init - pixi application & world initialization
 *
 * This function initializes pixi application
 * and game world @matrix which is d by d matrix
 */
const init = () => {
	let type = "WebGL";
	if (!PIXI.utils.isWebGLSupported()) {
		type = "canvas";
	}

	game = new PIXI.Application({
		width: gs,
		height: gs,
		backgroundColor: gameBgColor,
		antialias: true
	});

	/* pixi-parcel hmr hoops */
	let parent = document.getElementById("pixi-parent");
	let appContainer = document.getElementById("pixi-app-container");
	game.view.setAttribute("id", "pixi-app-container");
	parent.replaceChild(game.view, appContainer);

	matrix = Array.from({ length: d * d }, () => 0);
	done = false;
	lastRandomSequence = [];
	pickedup = false;
	score = 0;

	scoreEl = document.getElementsByClassName("score")[0];
	scoreEl.innerHTML = "score: " + score;

	scoreEl.addEventListener("score-update", () => {
		scoreEl.innerHTML = "score: " + score;
	});
};

/*
 * stateUpdate - updates game state
 */
const stateUpdate = () => {
	let freespace = false;
	for (let i = 0; i < d * d; i++) {
		freespace = freespace || !matrix[i];
	}

	if (!freespace) {
		let matches = [];
		for (let i = 0; i < d - 1; i++) {
			for (let j = 0; j < d - 1; j++) {
				let temp;
				temp = matrix[j + i * d];
				matrix[j + i * d] = matrix[j + 1 + i * d];
				matrix[j + 1 + i * d] = temp;

				let chains = findChains();
				if (chains.length) matches.push(chains);

				temp = matrix[j + i * d];
				matrix[j + i * d] = matrix[j + 1 + i * d];
				matrix[j + 1 + i * d] = temp;

				temp = matrix[j * d + i];
				matrix[j * d + i] = matrix[(j + 1) * d + i];
				matrix[(j + 1) * d + i] = temp;

				chains = findChains();
				if (chains.length) matches.push(chains);

				temp = matrix[j * d + i];
				matrix[j * d + i] = matrix[(j + 1) * d + i];
				matrix[(j + 1) * d + i] = temp;
			}
		}
		if (!matches.length) done = true;
	}

	for (let i = 0; i < d * d; i++) figureSink(i);

	for (let i = 0; i < d; i++) {
		if (matrix[i]) continue;
		let rv;
		do {
			rv = Math.floor(Math.random() * 3);
		} while (lastRandomSequence[i] === rv);
		while (
			rv === lastRandomSequence[Math.abs(i - 1)] ||
			rv == lastRandomSequence[i]
		) {
			rv = Math.floor(Math.random() * 3);
		}
		lastRandomSequence[i] = rv;
		figureCreate(shapeTypes[rv], i % d, Math.floor(i / d));
	}
	let chains = findChains();
	if (!chains) return;
	for (let i = 0; i < chains.length; i++) {
		if (!matrix[chains[i]]) return;
		score += matrix[chains[i]].points;
		matrix[chains[i]].Graphics.destroy();
		matrix[chains[i]] = 0;
	}

	scoreEl.dispatchEvent(new Event("score-update"));
};

/* graphicsUpdate - draws world changes
 * @delta: delta time since last frame
 *
 * traverses matrix, draws world according to present state,
 * moves figures with ms movement speed taking delta into account
 */
const graphicsUpdate = delta => {
	matrix.forEach(cell => {
		if (!cell) return;
		if (cell.Graphics.x < cell.x * fs) {
			let dx = cell.Graphics.x + ms * delta;
			cell.Graphics.position.set(
				dx > cell.x * fs ? cell.x * fs : dx,
				cell.Graphics.y
			);
		}
		if (cell.Graphics.y < cell.y * fs) {
			let dy = cell.Graphics.y + ms * delta;
			cell.Graphics.position.set(
				cell.Graphics.x,
				dy > cell.y * fs ? cell.y * fs : dy
			);
		}
	});
};

/*
 * loop - game loop
 * @delta: delta time since last frame
 *
 * This function updates world state
 * in the end drawing new one to the user
 */
const loop = delta => {
	if (done) {
		console.log("game over");
		game.ticker.stop();
	}

	stateUpdate();
	graphicsUpdate(delta);
};

(async () => {
	init();
	game.ticker.add(loop);
})();
